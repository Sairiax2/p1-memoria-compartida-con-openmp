#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

#define FILE_PATH 1
#define CARD_NAME 2

/* Output colours */
#define BLUE "\x1B[34m"
#define RED "\033[1;31m"
#define YELLOW "\033[1;33m"
#define MAGENTA "\x1B[35m"
#define RESET_COLOUR "\033[0m"

#define TRUE 1
#define FALSE 0

#define NROWS 0
#define MAX_LENGTH 1
#define TOTAL_TABS_PER_ROW 38

/* Columns with relevant information */
#define NAME 0
#define CMC 5
#define TYPE 8
#define RARITY 11
#define URL 32

// Global variables
int argc;
char** argv;
int fileLength;
int hits = -1;

int set_file_length(FILE* ptr)
{
    fseek(ptr, 0, SEEK_END)+1;      // Go to the end
    fileLength = ftell(ptr);        // Get the length
    fseek(ptr, 0, SEEK_SET);        // Go back to the beginning
}

int check_args()
{
    if(argc < 3)
    {
        printf("%sUsage: %s {path to file} {name of card}%s\n", RED, argv[0],
            RESET_COLOUR);
        exit(-1);
    }
}

void check_file(FILE* ptr)
{
    if (ptr == NULL) 
    {
		printf("%sERROR: File %s can't be opened!%s\n", RED, 
            argv[FILE_PATH], RESET_COLOUR);
        exit(-1);
	}
}

void print_card_name()
{
    printf("%s ---> Cards with a name containing: %s", YELLOW, MAGENTA);
    for(int i=CARD_NAME; i<argc; i++)
        printf("%s ", argv[i]);
    printf("%s\n", RESET_COLOUR);
}

int get_nrows()
{
    char ch;
    int nrows = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");

    for(int i=0; i<fileLength; i++)
    {
        ch = fgetc(ptr);

        if(ch == '\n')
            nrows++;
    }

    fclose(ptr);
    return nrows;
}

void fill_cards_pos(int cardsPos[])
{
    char ch;
    int nrows = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");

    int i;
    for(i=0; i<fileLength; i++)
    {
        ch = fgetc(ptr);

        if(ch == '\n')
        {
            cardsPos[nrows] = i; 
            nrows++;
        }
    }

    fclose(ptr);
}

void printDetails(int cardPos, char* file)
{
    char ch;
    FILE* ptr = fopen(file, "r");
    fseek(ptr, cardPos+1, SEEK_SET); // From the beginning of the file to the nth pos
    int tabCount = 0;

    printf("  ■ NAME: ");
    do
    {
        ch = fgetc(ptr);

        if(ch == '\t')
        {
            tabCount++;
            if(tabCount == CMC)
                printf("\n  ■ COST: ");
            else if(tabCount == TYPE)
                printf("\n  ■ TYPE: ");
            else if(tabCount == RARITY)
                printf("\n  ■ RARITY: ");
            else if(tabCount == URL)
                printf("\n  ■ URL: %s", BLUE);
        }
        else if(tabCount == CMC || tabCount == TYPE 
            || tabCount == RARITY || tabCount == URL || tabCount == NAME)
        {
            printf("%c", ch);
        }
    }
    while(ch != '\n');
    printf("%s\n\n", RESET_COLOUR);

    fclose(ptr);
}

void search_and_print(int cardsPos[], int nrows)
{
    char ch;
    int currentRow = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");
    int currentWord = CARD_NAME;
    int currentChar = 0;

    do
    {
        ch = fgetc(ptr);
        if(ch == '\t')
        {
            // End of card name, next row
            currentRow++;
            fseek(ptr, cardsPos[currentRow], SEEK_SET);
            currentWord = CARD_NAME;
            currentChar = 0;
        }
        else
        {
            if(ch == argv[currentWord][currentChar])
                currentChar++;
            else
                currentChar = 0;
            
            if(argv[currentWord][currentChar] == '\0')
            {
                /* Card found */
                if(argc == currentWord+1)
                {
                    hits++;
                    printf("[%d] #char %d -- #row %d\n", hits,
                        cardsPos[currentRow], currentRow+1);
                    printDetails(cardsPos[currentRow], argv[FILE_PATH]);
                }
                else
                {
                    currentWord++;
                    currentChar = 0;
                }
            }
        }
    }
    while(nrows != currentRow);

    fclose(ptr);

    printf("%s ---> %s%d%s cards found.%s\n", YELLOW, MAGENTA,
            hits, YELLOW, RESET_COLOUR);
}

void find_card(FILE* ptr)
{
    int nrows = get_nrows(fileLength);
    printf("%s ---> Rows counted: %s%d%s\n", YELLOW, MAGENTA,
        nrows+1, RESET_COLOUR);

    int cardsPos[nrows];
    fill_cards_pos(cardsPos);

    printf("\n -- RESULTS --\n");
    search_and_print(cardsPos, nrows);
}

int main(int ac, char* av[])
{
    argc = ac;
    argv = av;
    double start_time, time;
    FILE* ptr;

    start_time = omp_get_wtime();   // Start measuring time elapsed

    /* Checking if user has given the required arguments */
    check_args();
    /* Opening the file in reading mode */
    ptr = fopen(argv[FILE_PATH], "r");
    check_file(ptr);

    /* Showing the file length */
    set_file_length(ptr);
    printf("%s ---> %d characters detected.%s\n", YELLOW,
        fileLength, RESET_COLOUR);

    /* Showing the card name to be searched */
    print_card_name();
    
    find_card(ptr);
    
    /*Closing the file*/
	fclose(ptr);

    time = omp_get_wtime() - start_time;    // "Stop timer"
    printf("-----------------------------\n");
    printf("Elapsed Time: %fs", time);
    printf("\n-----------------------------\n");

	exit(0);
}