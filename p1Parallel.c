#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

#define FILE_PATH 1
#define CARD_NAME 2

/* Output colours */
#define BLUE "\x1B[34m"
#define RED "\033[1;31m"
#define YELLOW "\033[1;33m"
#define MAGENTA "\x1B[35m"
#define RESET_COLOUR "\033[0m"

#define TRUE 1
#define FALSE 0

#define NROWS 0
#define MAX_LENGTH 1
#define TOTAL_TABS_PER_ROW 38

/* Columns with relevant information */
#define NAME 0
#define CMC 5
#define TYPE 8
#define RARITY 11
#define URL 32

#define NTHREADS 100

// Global variables
int argc;
char** argv;
int fileLength;
int partialResults[NTHREADS];
int nThreads = 0;
int start[NTHREADS];
int end[NTHREADS];
int hits = -1;

int set_file_length(FILE* ptr)
{
    fseek(ptr, 0, SEEK_END)+1;      // Go to the end
    fileLength = ftell(ptr);        // Get the length
    fseek(ptr, 0, SEEK_SET);        // Go back to the beginning
}

int check_args()
{
    if(argc < 3)
    {
        printf("%sUsage: %s {path to file} {name of card}%s\n", RED, argv[0],
            RESET_COLOUR);
        exit(-1);
    }
}

void check_file(FILE* ptr)
{
    if (ptr == NULL) 
    {
		printf("%sERROR: File %s can't be opened!%s\n", RED, 
            argv[FILE_PATH], RESET_COLOUR);
        exit(-1);
	}
}

void print_card_name()
{
    printf("%s ---> Cards with a name containing: %s", YELLOW, MAGENTA);
    for(int i=CARD_NAME; i<argc; i++)
        printf("%s ", argv[i]);
    printf("%s\n", RESET_COLOUR);
}

int parallel_get_nrows(int start, int end)
{
    char ch;
    int nrows = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");
    fseek(ptr, start, SEEK_SET);

    for(int i=start; i<end; i++)
    {
        ch = fgetc(ptr);

        if(ch == '\n')
            nrows++;
    }

    fclose(ptr);
    return nrows;
}

int get_nrows()
{
    int nrows = 0;

    #pragma omp parallel num_threads(NTHREADS)
    {
        // Calculating the job that each thread must perform
        #pragma omp single
        {
            nThreads = omp_get_num_threads();
            int split = fileLength / nThreads;
            int previousEnd = -1;

            for(int i=0; i<nThreads-1;i++)
            {
                start[i] = previousEnd + 1;
                previousEnd += split;
                end[i] = previousEnd;
            }

            start[nThreads-1] = previousEnd + 1;
            end[nThreads-1] = fileLength;
        }

        int id = omp_get_thread_num();
        partialResults[id] = parallel_get_nrows(start[id], end[id]);
    }

    for(int i=0; i<nThreads; i++)
        nrows += partialResults[i];

    return nrows;
}

void parallel_fill_cards_pos(int start, int end, int auxPos[])
{
    char ch;
    int nrows = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");
    fseek(ptr, start, SEEK_SET);

    if(start == 0) nrows++;     // Fix first row being taken for granted

    int i;
    for(i=start; i<end; i++)
    {
        ch = fgetc(ptr);

        if(ch == '\n')
        {
            auxPos[nrows] = i; 
            nrows++;
        }
    }

    fclose(ptr);
}

int max_rows_per_thread()
{
    int max = 0;
    
    for(int i=0; i<nThreads; i++)
    {
        if(max < partialResults[i])
            max = partialResults[i];
    }

    return max;
}

void fill_cards_pos(int cardsPos[])
{
    int auxPos[NTHREADS][max_rows_per_thread()+1];

    #pragma omp parallel num_threads(NTHREADS)
    {
        // I need the same number of threads as last time
        #pragma omp single
        {
            int numberThreads = omp_get_num_threads();
            if(numberThreads != nThreads)
                exit(-1);
        }

        int id = omp_get_thread_num();
        parallel_fill_cards_pos(start[id], end[id], auxPos[id]);
    }

    int currentRow = 0;
    for(int i=0; i<nThreads; i++)
    {
        for(int j=0; j<partialResults[i]; j++)
        {
            cardsPos[currentRow] = auxPos[i][j];
            currentRow++;
        }
    }
}

void printDetails(int cardPos, char* file)
{
    char ch;
    FILE* ptr = fopen(file, "r");
    fseek(ptr, cardPos+1, SEEK_SET); // From the beginning of the file to the nth pos
    int tabCount = 0;

    printf("  ■ NAME: ");
    do
    {
        ch = fgetc(ptr);

        if(ch == '\t')
        {
            tabCount++;
            if(tabCount == CMC)
                printf("\n  ■ COST: ");
            else if(tabCount == TYPE)
                printf("\n  ■ TYPE: ");
            else if(tabCount == RARITY)
                printf("\n  ■ RARITY: ");
            else if(tabCount == URL)
                printf("\n  ■ URL: %s", BLUE);
        }
        else if(tabCount == CMC || tabCount == TYPE 
            || tabCount == RARITY || tabCount == URL || tabCount == NAME)
        {
            printf("%c", ch);
        }
    }
    while(ch != '\n');
    printf("%s\n\n", RESET_COLOUR);

    fclose(ptr);
}

void parallel_search_and_print(int start, int end, int cardsPos[])
{
    char ch;
    int nrows = 0;
    FILE* ptr = fopen(argv[FILE_PATH], "r");
    fseek(ptr, cardsPos[start], SEEK_SET);
    int currentWord = CARD_NAME;
    int currentChar = 0;

    do
    {
        ch = fgetc(ptr);
        if(ch == '\t')
        {
            // End of card name, next row
            nrows++;
            fseek(ptr, cardsPos[nrows+start], SEEK_SET);
            currentWord = CARD_NAME;
            currentChar = 0;
        }
        else
        {
            if(ch == argv[currentWord][currentChar])
                currentChar++;
            else
                currentChar = 0;
            
            if(argv[currentWord][currentChar] == '\0')
            {
                /* Card found */
                if(argc == currentWord+1)
                {
                    #pragma omp critical
                    {
                        hits++;
                        printf("[%d] #char %d -- #row %d\n", hits,
                            cardsPos[nrows+start], nrows+start+1);
                        printDetails(cardsPos[nrows+start], argv[FILE_PATH]);
                    }
                }
                else
                {
                    currentWord++;
                    currentChar = 0;
                }
            }
        }
    }
    while(nrows != end-start);

    fclose(ptr);
}

void search_and_print(int cardsPos[], int nrows)
{
    int nThreads = 0;
    int startRow[NTHREADS];
    int endRow[NTHREADS];

    #pragma omp parallel num_threads(NTHREADS)
    {
        // Calculating the job for each thread
        #pragma omp single
        {
            nThreads = omp_get_num_threads();
            int split = nrows / nThreads;
            int previousEnd = -1;

            for(int i=0; i<nThreads-1;i++)
            {
                startRow[i] = previousEnd + 1;
                previousEnd += split;
                endRow[i] = previousEnd;
            }

            startRow[nThreads-1] = previousEnd + 1;
            endRow[nThreads-1] = nrows;
        }

        int id = omp_get_thread_num();
        parallel_search_and_print(startRow[id], endRow[id], cardsPos);
    }

    printf("%s ---> %s%d%s cards found.%s\n", YELLOW, MAGENTA,
            hits, YELLOW, RESET_COLOUR);
}

void find_card(FILE* ptr)
{
    int nrows = get_nrows(fileLength);
    printf("%s ---> Rows counted: %s%d%s\n", YELLOW, MAGENTA,
        nrows+1, RESET_COLOUR);

    int cardsPos[nrows];
    fill_cards_pos(cardsPos);

    printf("\n -- RESULTS --\n");
    search_and_print(cardsPos, nrows);
}

int main(int ac, char* av[])
{
    argc = ac;
    argv = av;
    double start_time, time;
    FILE* ptr;

    start_time = omp_get_wtime();   // Start measuring time elapsed
    /* Checking if user has given the required arguments */
    check_args();
    /* Opening the file in reading mode */
    ptr = fopen(argv[FILE_PATH], "r");
    check_file(ptr);

    #pragma omp parallel sections
    {
        #pragma omp section
        {
            /* Showing the file length */
            set_file_length(ptr);
            printf("%s ---> %d characters detected.%s\n", YELLOW,
                fileLength, RESET_COLOUR);
        }

        #pragma omp section
        /* Showing the card name to be searched */
        print_card_name();
    }
    
    find_card(ptr);
    
    /*Closing the file*/
	fclose(ptr);

    time = omp_get_wtime() - start_time;    // "Stop timer"
    printf("-----------------------------\n");
    printf("Elapsed Time: %fs", time);
    printf("\n-----------------------------\n");

	exit(0);
}